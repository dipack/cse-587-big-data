#!/usr/bin/env python
from __future__ import print_function
import sys

"""
Reduces the generated word pairs received from the mapper
See example in `wc_mapper.py`
"""

tracked_pair = None
current_pair = None
counter = 0

for line in sys.stdin:
    line = line.strip()
    try:
        current_pair, current_count = line.split('\t')
        current_count = int(current_count)
    except ValueError:
        current_pair = None
        continue

    if tracked_pair == current_pair:
        counter += current_count
    else:
        if tracked_pair:
            print('%s\t%s' % (tracked_pair, counter))
        counter = current_count
        tracked_pair = current_pair

if tracked_pair == current_pair:
    print("{0}\t{1}".format(tracked_pair, counter))
