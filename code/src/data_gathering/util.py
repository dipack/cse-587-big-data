from datetime import datetime
from dateutil.relativedelta import relativedelta
from nltk.stem import SnowballStemmer

Topics = {
    'football': {
        'twitter': 'football OR soccer',
        'nyt': 'football+soccer',
        'cc': ['football', 'soccer']
    },
    'basketball': {
        'twitter': 'basketball OR ncaa OR nba',
        'nyt': 'basketball',
        'cc': ['basketball', 'nba', 'ncaa']
    },
    'cricket': {
        'twitter': 'cricket OR ipl OR t20',
        'nyt': 'cricket',
        'cc': ['cricket', 'ipl', 't20']
    },
    'golf': {
        'twitter': 'golf OR pga',
        'nyt': 'golf',
        'cc': ['golf', 'pga']
    },
    'tennis': {
        'twitter': 'tennis OR australian open OR us open OR french open',
        'nyt': 'tennis',
        'cc': ['tennis', 'australian open', 'us open', 'french open']
    }
}


def get_topic(source='twitter', topic='usa'):
    filename, query_str = None, None
    if topic not in Topics or source not in Topics[topic]:
        return filename, query_str
    base_path = "../../data/{0}".format(source)
    filename = "{0}_{1}.csv".format(source, topic)
    stemmed = "{0}/stemmed/{1}".format(base_path, filename)
    non_stemmed = "{0}/to_stem/{1}".format(base_path, filename)
    query_str = Topics[topic][source]
    if source == 'twitter':
        query_str += " -filter:retweets AND -filter:replies"
    return stemmed, non_stemmed, query_str


def rewind_clock(date: 'str', years=-1) -> 'str':
    date_fmt = "%Y%m%d"
    old_date = datetime.strptime(date, date_fmt).date()
    new_date = old_date + relativedelta(years=years)
    return datetime.strftime(new_date, date_fmt)


def clean_word(word):
    stripped = "".join(ch.lower() for ch in word if ch.isalpha())
    return stripped


def stem_str(to_stem):
    stemmer = SnowballStemmer("english")
    cleaned = " ".join([stemmer.stem(clean_word(x)) for x in to_stem.split()])
    return cleaned
