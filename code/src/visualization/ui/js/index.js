const TOPICS = ['football', 'cricket', 'basketball', 'golf', 'tennis'];
const DATA_SOURCES = ['twitter', 'nyt', 'cc'];
const TECH = {WORD_COUNT: 'wc', WORD_CO_OCCURRENCE: 'wo'};
const DATA_STORE = {[TECH.WORD_COUNT]: {}, [TECH.WORD_CO_OCCURRENCE]: {}};
const WO_TOP = 20;

// const LoadingSpinner = document.getElementById('loading-spinner');
const LoadingSpinner = document.getElementById('overlay');
const showSpinner = () => LoadingSpinner.style.display = 'block';
const hideSpinner = () => LoadingSpinner.style.display = 'none';

const DataSourceDropdown = document.getElementById('data-source');
const getDataSource = () => DataSourceDropdown.value;

const DataTopicDropdown = document.getElementById('data-topic');
const getDataTopic = () => DataTopicDropdown.value;

const WoSourceDropdown = document.getElementById('wo-source');
const getWoDataSource = () => WoSourceDropdown.value;

const WoTopicDropdown = document.getElementById('wo-topic');
const getWoDataTopic = () => WoTopicDropdown.value;

const WoWordsSelect = document.getElementById('wo-words');
const getWoWord = () => WoWordsSelect.value;

const WoWordCloudBtn = document.getElementById('wo-word-cloud-btn');

const getTop = () => 500;

const onLoad = () => {
    console.log('Startup');
    fetchAllData();
};

function makeRequest(method, url) {
    return new Promise(function (resolve, reject) {
        const xhr = new XMLHttpRequest();
        xhr.open(method, url);
        xhr.onload = function () {
            if (this.status >= 200 && this.status < 300) {
                resolve(xhr.responseText);
            } else {
                reject({
                    status: this.status,
                    statusText: xhr.statusText
                });
            }
        };
        xhr.onerror = function () {
            reject({
                status: this.status,
                statusText: xhr.statusText
            });
        };
        xhr.send();
    });
}

const fetchWordCountData = (source, topic, top) => {
    const tech = TECH.WORD_COUNT;
    const url = `http://localhost:9000/${tech}/${source}/${topic}/${top}`;
    return makeRequest('GET', url)
        .then((jsonStr) => {
            const data = JSON.parse(jsonStr);
            if (!DATA_STORE[tech].hasOwnProperty(source)) {
                DATA_STORE[tech][source] = {};
            }
            DATA_STORE[tech][source][topic] = data;
            console.log(`Successfully fetched and stored data for ${tech} - ${topic} from ${source}`);
        })
        .catch((e) => {
            console.error(e);
            return e;
        });
};

const fetchAllData = () => {
    showSpinner();
    const promises = [];
    TOPICS.forEach(topic => {
        DATA_SOURCES.forEach(src => {
            promises.push(fetchWordCountData(src, topic, getTop()));
        })
    });
    return Promise.all(promises)
        .then(() => {
            hideSpinner();
        })
        .catch(() => {
            hideSpinner();
        })
};

const fetchWordCoWords = (source, topic) => {
    const words = DATA_STORE[TECH.WORD_COUNT][source][topic];
    const topTwenty = filterTop(words, WO_TOP);
    console.log(`Top 20 words ${source}, ${topic}  are`, topTwenty);
    WoWordsSelect.innerHTML = '';
    topTwenty.forEach((word) => {
        const option = document.createElement('option');
        option.value = word.text;
        option.textContent = `${word.text} - ${word.size}`;
        WoWordsSelect.appendChild(option);
    });
    WoWordCloudBtn.disabled = false;
};

const filterTop = (data, topValues) => {
    return data.sort(function (a, b) {
        return a.size < b.size ? 1 : -1;
    }).slice(0, topValues);
};

const drawWoCloud = (source, topic, word) => {
    const tech = TECH.WORD_CO_OCCURRENCE;
    const top = WO_TOP;
    const url = `http://localhost:9000/${tech}/${source}/${topic}/${word}/${top}`;
    return makeRequest('GET', url)
        .then((jsonStr) => {
            const data = JSON.parse(jsonStr);
            if (!DATA_STORE[tech].hasOwnProperty(source)) {
                DATA_STORE[tech][source] = {};
            }
            DATA_STORE[tech][source][topic] = data;
            console.log(`Successfully fetched and stored data for ${tech} - ${topic} from ${source}`);
            drawFor(tech, source, topic);
        })
        .catch((e) => {
            console.error(e);
            hideSpinner();
            return e;
        });

}

const drawFor = (tech, source, topic) => {
    const tag = tech === TECH.WORD_COUNT ? 'wc-word-cloud-box' : 'wo-word-cloud-box';
    const el = document.getElementById(tag);
    // Copied from here https://bl.ocks.org/jyucsiro/767539a876836e920e38bc80d2031ba7
    console.log(`Drawing cloud for ${topic} from ${source}`);
    showSpinner();

    // Quick way to clone
    const words = JSON.parse(JSON.stringify(DATA_STORE[tech][source][topic]));

    const svg_location = `#${tag}`;
    const width = 800;
    const height = 300;

    const fill = d3.scale.category20();

    const xScale = d3.scale.linear()
        .domain([
            0,
            d3.max(words, function (d) {
                return d.size;
            })
        ])
        // Modify these values to increase word size
        .range([40, 130]);

    d3.layout
        .cloud()
        .size([width, height])
        .timeInterval(20)
        .words(words)
        .fontSize(function (d) {
            return xScale(+d.size);
        })
        .text(function (d) {
            return d.text;
        })
        .rotate(function () {
            return 0;
            // return ~~(Math.random() * 2) * 90;
        })
        .font("Impact")
        .on("end", draw)
        .start();

    function draw(words) {
        el.innerHTML = '';
        d3.select(svg_location)
            .append("svg")
            .attr("width", width)
            .attr("height", height)
            .append("g")
            .attr("transform", "translate(" + [width >> 1, height >> 1] + ")")
            .selectAll("text")
            .data(words)
            .enter().append("text")
            .style("font-size", function (d) {
                return xScale(d.size) + "px";
            })
            .style("font-family", "Impact")
            .style("fill", function (d, i) {
                return fill(i);
            })
            .attr("text-anchor", "middle")
            .attr("transform", function (d) {
                return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
            })
            .text(function (d) {
                return d.text;
            });
    }

    d3.layout.cloud().stop();

    hideSpinner();
};
