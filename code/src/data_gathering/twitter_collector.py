import collections
import logging
import os

import pandas as pd
import twitter
from util import get_topic, stem_str

logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p',
                    level=logging.INFO)


class Tweeter:
    def __init__(self):
        self.api = twitter.Api(
            consumer_key="o4L8fmcUaS7zV0FltrgjX0IBT",
            consumer_secret="rVt8yDYT3kdQbm98edL6PbXtSrhhuKZSN7a4SdZDPuc13icrdu",
            access_token_key="1093611197486768128-u8l8NVnQJm3EZKWH2b27k0oTyfsWfH",
            access_token_secret="C5FUlBXgwk28em2ugDnemwU9VpyMobWlcF9L071jzd7LR",
            sleep_on_rate_limit=True
        )
        # Not saving metadata
        # self.col_names = ["id", "user", "timestamp", "text", "location"]
        self.col_names = ["id", "text"]
        self.tweets = None

    def search(self, term, target_tweet_count=20):
        logging.info("Searching twitter using term: {0}, for {1} tweets".format(term, target_tweet_count))
        max_id = 0
        if self.tweets is not None:
            max_id = min(self.tweets.index) - 1
        results = self.api.GetSearch(term=term, lang="en", count=target_tweet_count, max_id=max_id)
        tweets_d = collections.defaultdict(list)
        for tweet in results:
            # Not saving metadata
            extracted = [
                tweet.id,
                # tweet.user.screen_name,
                # str(tweet.created_at),
                stem_str(str(tweet.text).replace('\n', ' ').replace('\r', ''))
                # tweet.location
            ]
            for k, v in zip(self.col_names, extracted):
                tweets_d[k].append(v)
        if self.tweets is None:
            self.tweets = pd.DataFrame.from_dict(tweets_d)
            self.tweets.set_index('id', inplace=True)
        else:
            if len(tweets_d):
                df = pd.DataFrame.from_dict(tweets_d)
                df.set_index('id', inplace=True)
                self.tweets = self.tweets.append(df)
        self.tweets.replace('\n', ' ', regex=True, inplace=True)
        self.tweets.drop_duplicates(inplace=True)
        self.tweets.dropna(inplace=True)
        logging.info("Found {0} unique tweets".format(len(self.tweets)))
        return self.tweets

    def save_tweets(self, tweet_filename="data/tweets.csv"):
        if self.tweets is not None:
            logging.info("Saving tweets to file {0}".format(tweet_filename))
            self.tweets.to_csv(tweet_filename, sep=",", header=False, index=False)
        return


def main():
    # sleep_time = 15 * 60
    counter = 0
    target_tweet_count = 200
    twit = Tweeter()

    stemmed_filename, _, query_str = get_topic('twitter', 'tennis')

    # As we don't store any metadata, we cannot continue from where we left off sadly..
    # if os.path.isfile(stemmed_filename):
    #     twit.tweets = pd.read_csv(stemmed_filename, header=None, index_col=False)
    #     logging.info(
    #         "Starting from where we left off before {0} with {1} tweets".format(stemmed_filename, len(twit.tweets)))

    tweets = twit.search(query_str, target_tweet_count)
    counter += 1
    while len(twit.tweets) < target_tweet_count and counter < 10:
        twit.save_tweets(stemmed_filename)
        # logging.info("Sleeping for {0} seconds".format(sleep_time))
        # time.sleep(sleep_time)
        # logging.info("Awake!")
        twit.search(query_str, target_tweet_count)
    print(tweets.head())
    twit.save_tweets(stemmed_filename)
    return


if __name__ == "__main__":
    main()
