# Python 2.6 compat
from __future__ import print_function
import sys
import csv
from optparse import OptionParser


def setup_options():
    parser = OptionParser()
    parser.add_option("-t", "--topic", action="store", dest="topic", default="football",
                      help="Topic to count words for")
    parser.add_option("-s", "--source", action="store", dest="source", default="nyt",
                      help="Source data to use for counting words")
    parser.add_option("-n", "--number", action="store", dest="number", default=20, help="Top N words to count")
    return parser


def parse_args(args):
    (options, args) = setup_options().parse_args(args)
    topic = options.topic
    source = options.source
    number = int(options.number)
    return topic, source, number


def main():
    topic, source, number = parse_args(sys.argv[1:])
    base_path = "../../results/{0}/{1}".format(source, topic)
    data_file_path = "{0}/part-00000".format(base_path)
    out_file_path = "{0}/top_{1}.csv".format(base_path, number)
    words = []
    lines = []
    print("Reading mapreduced data from {0}".format(data_file_path))
    with open(data_file_path, "r") as fi:
        lines = fi.readlines()
    for line in lines:
        try:
            word, count = line.split("\t")
            count = int(count)
            words.append({'text': word, 'size': count})
        except ValueError:
            continue
    print("Sorting words based on count, selecting TOP {0}".format(number))
    sorted_words = sorted(words, key=lambda x: x['size'], reverse=True)
    max_len = min(number, len(sorted_words))
    target_words = sorted_words[:max_len]
    print("Writing the TOP {0} to {1}".format(number, out_file_path))
    with open(out_file_path, 'wb') as csvfile:
        writer = csv.writer(csvfile, delimiter='|', quoting=csv.QUOTE_NONE, quotechar='')
        for word in target_words:
            writer.writerow(["{0},{1}".format(word['text'], word['size'])])
    return target_words


if __name__ == '__main__':
    main()
