import os
import json
import requests
import logging
import numpy as np
import pandas as pd
from gzip import GzipFile
from io import StringIO, BytesIO
from bs4 import BeautifulSoup as bs
from util import get_topic

logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p',
                    level=logging.INFO)


# TODO: Stem words while saving
class CommonCrawler:
    def __init__(self):
        self.indexes = ["2019-09", "2019-04", "2018-51", "2018-47", "2018-43", "2018-39", "2018-34", "2018-30",
                        "2018-26"]
        self.indexes = ["2019-09", "2019-04"]
        self.crawled = []
        pass

    def search_for_domain(self, domain="nytimes.com"):
        records = []
        logging.info("Crawling for domain {0}".format(domain))
        for idx in self.indexes:
            logging.info("Crawling index: {0}".format(idx))
            url = "http://index.commoncrawl.org/CC-MAIN-{0}-index?url={1}&matchType=domain&output=json".format(idx,
                                                                                                               domain)
            response = requests.get(url)
            if response.status_code == 200:
                response_records = response.content.splitlines()
                for rr in response_records:
                    records.append(json.loads(rr))
        logging.info("Found {0} records over all indexes".format(len(records)))
        return records

    def extract_html(self, record):
        offset, length = int(record['offset']), int(record['length'])
        offset_end = offset + length - 1
        filename = record['filename']
        url = "https://commoncrawl.s3.amazonaws.com/{0}".format(filename)
        try:
            response = requests.get(url, headers={'Range': 'bytes={0}-{1}'.format(offset, offset_end)})
            if response.status_code == 200 or response.status_code == 206:
                # raw_data = StringIO(response.content)
                raw_data = BytesIO()
                raw_data.write(response.content)
                raw_data.seek(0)
                with GzipFile(fileobj=raw_data, mode='rb') as file:
                    data = file.read()
                    if len(data):
                        try:
                            warc, header, response = data.strip().decode('ISO-8859-1').split('\r\n\r\n', 2)
                            # logging.info("Successfully read file")
                        except ValueError as e:
                            response = None
                            logging.error(e)
                            logging.error("Failed to read data from {0}".format(url))
            else:
                response = None
                logging.error("Could not read file from url: {0}".format(url))
        except Exception as e:
            response = None
            logging.error(e)
            logging.error("Could not read file from url: {0}".format(url))
        return response

    def extract_external_links(self, html, domain, links):
        soup = bs(html)
        anchors = soup.find_all("a")
        if anchors:
            for link in anchors:
                href = link.attrs.get("href")
                if href is not None and domain not in href:
                    if href not in links and href.startswith("http"):
                        logging.info("Discovered external link: {0}".format(href))
                        links.append(href)
        return links

    def extract_relevant_info(self, html, terms):
        soup = bs(html, features="html.parser")
        found = False
        text = None
        if soup:
            for script in soup(['script', 'style']):
                script.decompose()
            for term in terms:
                if term in soup.get_text():
                    found = True
                    break
        if found:
            text = soup.get_text()
        return text

    def search(self, terms, domain, max_records=10000):
        logging.info("Retrieving archives for domain {0}".format(domain))
        records = self.search_for_domain(domain)
        logging.info("Looking for terms {0} in retrieved records".format(terms))
        max_len = min(max_records, len(records))
        # max_len = 50
        for idx, record in enumerate(records[:max_len]):
            if idx % 100 == 0:
                logging.info("Processing record {0}".format(idx))
            html = self.extract_html(record)
            if html is not None:
                text = self.extract_relevant_info(html, terms)
                if text is not None:
                    text_cleaned = " ".join(text.replace('\n', ' ').replace('\r', '').split())
                    self.crawled.append(text_cleaned)
        self.crawled = np.unique(self.crawled)
        logging.info("Having {0} records matching search terms".format(len(self.crawled)))
        return self.crawled

    def save_texts(self, target_filename="data/cc_texts.csv"):
        if len(self.crawled):
            logging.info("Saving common crawled data to file {0}".format(target_filename))
            pd.DataFrame(self.crawled).to_csv(target_filename, index=None, header=None)
        return


def main():
    counter = 0
    max_records = 100
    domain = "espn.com"
    filename, terms = get_topic('cc', 'tennis')

    cc = CommonCrawler()

    if os.path.isfile(filename):
        with open(filename, 'r') as f:
            cc.crawled = f.readlines()
            logging.info("Starting from where we left off {0} with {1} records".format(filename, len(cc.crawled)))

    cc.search(terms=terms, domain=domain)
    # counter += 1
    # while len(cc.crawled) < max_records and counter < 10:
    #     cc.save_texts(filename)
    #     cc.search(terms=terms, domain=domain)
    #     counter += 1
    cc.save_texts(filename)


if __name__ == '__main__':
    main()
