#!/usr/bin/env bash
# This script is meant to be used with the Cloudera Docker quickstart image
# In order to do so, please run the script `runner.sh`, in the `code/` directory
# By Dipack P Panjabi

local_mr_dir="/mr";
local_results_dir="$local_mr_dir/results/";
hdfs_user_dir="/user";
hdfs_mr_dir="$hdfs_user_dir/mr";
hdfs_results_dir="$hdfs_mr_dir/results";
hdfs_data_dir="$hdfs_mr_dir/data";

function map_reduce_task {
    echo "Enter the name of the topic you want to run the mapreduce for?";
    read -r -p "Choices: (football, cricket, golf, basketball, tennis) = " topic;
    case "$topic" in
        [fF][oO][oO][tT][bB][aA][lL][lL])
            topic="football"
            ;;
        [cC][rR][iI][cC][kK][eE][tT])
            topic="cricket"
            ;;
        [gG][oO][lL][fF])
            topic="golf"
            ;;
        [bB][aA][sS][kK][eE][tT][bB][aA][lL][lL])
            topic="basketball"
            ;;
        [tT][eE][nN][nN][iI][sS])
            topic="tennis"
            ;;
        *)
            echo "Invalid topic: $topic";
            exit 1;
            ;;
    esac
    echo "Topic chosen is $topic";
    _map_reduce_wc "$topic";

    copy_to_local;

    echo "Now we must pick the top N words from each source";
    read -r -p "Please enter a value for N = " number;
    if ! [[ "$number" =~ ^[0-9]+$ ]]; then
        echo "Sorry integers only";
        echo "Setting to 20";
        number=20;
    fi

    _map_reduce_wo "$topic" "$number";
}

function put_from_local {
    read -r -p "Should I delete, recreate HDFS location $hdfs_mr_dir and copy local directory $local_mr_dir to it? (y/n) = " can_delete_copy;
    case "$can_delete_copy" in
        [yY][eE][sS]|[yY])
            hdfs dfs -rm -r -f "$hdfs_mr_dir";
            hdfs dfs -mkdir -p "$hdfs_mr_dir";
            hdfs dfs -put "$local_mr_dir" "$hdfs_user_dir";
            ;;
        *)
            echo "Moving on then!";
            ;;
    esac
}

function copy_to_local {
    local_results_dir="$local_mr_dir/results/";
    read -r -p "Should I copy HDFS location $hdfs_results_dir to local $local_results_dir? (y/n) = " can_move;
    case "$can_move" in
        [yY][eE][sS]|[yY])
            hdfs dfs -get "$hdfs_results_dir" "$local_mr_dir";
            ;;
        *)
            echo "Not copying";
            ;;
    esac
}

function clean_results_dir {
    echo "HDFS Results directory $hdfs_results_dir needs to be deleted in order to work";
    read -r -p "Do you want to continue? (y/n) = " can_delete;
    case "$can_delete" in
        [yY][eE][sS]|[yY])
            hdfs dfs -rm -r -f "$hdfs_results_dir";
            ;;
        *)
            echo "Exiting!";
            exit 1;
            ;;
    esac
}

function _map_reduce_wc_cmd {
    topic="$1";
    source="$2";
    input="$hdfs_data_dir/$source/stemmed/*_$topic.csv";
    output="$hdfs_results_dir/$source/$topic";
    echo "Data sourced from $source";
    echo "Input files for Mapreduce are from HDFS location $input";
    echo "Output will be placed at HDFS location $output";
    hadoop jar /usr/lib/hadoop-0.20-mapreduce/contrib/streaming/hadoop-streaming-2.6.0-mr1-cdh5.7.0.jar \
        -file /mr/src/map_reduce/english_stopwords.txt \
        -file /mr/src/map_reduce/wc_mapper.py  -mapper /mr/src/map_reduce/wc_mapper.py \
        -file /mr/src/map_reduce/wc_reducer.py -reducer /mr/src/map_reduce/wc_reducer.py \
        -input "$input"  -output "$output";
}

function _map_reduce_wc {
    topic="$1";
    read -r -p "Run word count task for $topic? (y/n) = " can_run;
    case "$can_run" in
        [yY][eE][sS]|[yY])
            _map_reduce_wc_cmd "$topic" "cc";
            _map_reduce_wc_cmd "$topic" "nyt";
            _map_reduce_wc_cmd "$topic" "twitter";
            ;;
        *)
            echo "Moving on then!";
            ;;
    esac
}

function _map_reduce_wo_cmd {
    topic="$1";
    number="$2";
    source="$3";
    base_dir="/mr/src/map_reduce";
    change_dir_to="$base_dir/";
    present_dir="$(pwd)";
    echo "Switching to $change_dir_to";
    cd "$change_dir_to";
    /usr/bin/python "count_top.py" --topic "$topic" --source "$source" --number "$number";
    if [[ $? == 0 ]]; then
        echo "Successfully counted top $number for $topic from $source";
        move_to_dir="$base_dir/";
        file_to_move="$local_results_dir/$source/$topic/top_$number.csv";
        final_location="$move_to_dir/top_$number.csv";
        echo "Copying $file_to_move to $move_to_dir";
        cp "$file_to_move" "$move_to_dir";
        if [[ $? == 0 ]]; then
            hdfs_move_to_dir="$hdfs_user_dir/$move_to_dir";
            hdfs_file_location="$hdfs_move_to_dir/top_$number.csv";
            echo "Copying $file_to_move to HDFS location $hdfs_move_to_dir";
            hdfs dfs -rm -f "$hdfs_file_location";
            hdfs dfs -put "$file_to_move" "$hdfs_move_to_dir";
            input="$hdfs_data_dir/$source/stemmed/*_$topic.csv";
            output="$hdfs_results_dir/$source/$topic/wo";
            echo "Data sourced from $source";
            echo "Input files for Mapreduce are from HDFS location $input";
            echo "Output will be placed at HDFS location $output";
            hadoop jar /usr/lib/hadoop-0.20-mapreduce/contrib/streaming/hadoop-streaming-2.6.0-mr1-cdh5.7.0.jar \
                -file /mr/src/map_reduce/english_stopwords.txt \
                -file "$file_to_move" \
                -file /mr/src/map_reduce/wo_mapper.py  -mapper /mr/src/map_reduce/wo_mapper.py \
                -file /mr/src/map_reduce/wo_reducer.py -reducer /mr/src/map_reduce/wo_reducer.py \
                -input "$input"  -output "$output";
            echo "Removing $final_location";
            rm "$final_location";
        else
            echo "Could not copy";
        fi
        cd "$present_dir";
    else
        cd "$present_dir";
        echo "Failed in counting";
        echo "Exiting!";
        exit 2;
    fi
}

function _map_reduce_wo {
    topic="$1";
    number="$2";
    read -r -p "Run word co-occurrence task for $topic with the top $number words? (y/n) = " can_run;
    case "$can_run" in
        [yY][eE][sS]|[yY])
            _map_reduce_wo_cmd "$topic" "$number" "cc";
            _map_reduce_wo_cmd "$topic" "$number" "nyt";
            _map_reduce_wo_cmd "$topic" "$number" "twitter";
            ;;
        *)
            echo "Moving on then!";
            ;;
    esac
}

put_from_local;
clean_results_dir;
map_reduce_task;
copy_to_local;
exit 0;
