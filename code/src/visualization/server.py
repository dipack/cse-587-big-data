#!/usr/bin/env python3
from flask import Flask, jsonify, Response

app = Flask(__name__)


def count_words(source, topic, num):
    print("Want word count information for {0} from {1} data source with top {2} values".format(topic, source, num))
    location = "../../results/{0}/{1}/part-00000".format(source, topic)
    print("Reading file from location {0}".format(location))
    words = []
    lines = []
    with open(location, "r") as fi:
        lines = fi.readlines()
    for line in lines:
        try:
            word, count = line.split("\t")
            count = int(count)
            words.append({'text': word, 'size': count})
        except ValueError:
            continue
    sorted_words = sorted(words, key=lambda x: x['size'], reverse=True)
    max_len = min(num, len(sorted_words))
    return sorted_words[:max_len]


@app.route("/wc/<source>/<topic>", defaults={'num': 20}, methods=["GET"])
@app.route("/wc/<source>/<topic>/<num>", methods=["GET"])
def serve_wc_data(source, topic, num):
    num = int(num)
    return jsonify(count_words(source=source, topic=topic, num=num))


@app.route("/wo/<source>/<topic>/<topWord>", defaults={'num': 20}, methods=["GET"])
@app.route("/wo/<source>/<topic>/<topWord>/<num>", methods=["GET"])
def serve_wo_data(source, topic, topWord, num):
    num = int(num)
    print("Want word co-occurrence information for {0} from {1} data source with top {2} values"
          .format(topic, source, num))
    location = "../../results/{0}/{1}/wo/part-00000".format(source, topic)
    print("Reading file from location {0}".format(location))
    words = []
    lines = []
    with open(location, "r") as fi:
        lines = fi.readlines()
    for line in lines:
        try:
            pair, count = line.split("\t")
            a, b = pair.split(",")
            if a == topWord:
                word = b
                count = int(count)
                words.append({'text': word, 'size': count})
        except ValueError:
            continue
    sorted_words = sorted(words, key=lambda x: x['size'], reverse=True)
    max_len = min(num, len(sorted_words))
    return jsonify(sorted_words[:max_len])


@app.after_request
def allow_cross_domain(response: Response):
    """Hook to set up response headers."""
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Headers'] = 'content-type'
    return response


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=9000)
