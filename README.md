# CSE 587 - MapReduce Assignment
## Instructions:
1. The instructions on how to use this project, are mentioned in Section 2.4 `Toolchain` in the report.
2. The video demonstration for this project is `lab_2_demo.mp4` at the same level as this file
3. The report is `report.pdf` at the same level as this file

## TODO:
1. ~~Save output from data gatherers as pure text, stemmed~~


## Notes:
1. Logs on Hadoop docker image can be accessed via Hue, and are located at `/var/log/hadoop-yarn/apps/root/logs/`
