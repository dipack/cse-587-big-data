import os
import pandas as pd
from nltk.stem import SnowballStemmer


def clean_word(word):
    stripped = "".join(ch.lower() for ch in word if ch.isalpha())
    return stripped


def stem_data():
    stemmer = SnowballStemmer("english")
    base_path = "../../data"
    sources = ["cc", "nyt", "twitter"]
    inner_folder = "to_stem"
    output_folder = "stemmed"
    for source in sources:
        path = "{0}/{1}/{2}".format(base_path, source, inner_folder)
        output_path = "{0}/{1}/{2}".format(base_path, source, output_folder)
        if os.path.isdir(path):
            files = os.listdir(path)
            for f in files:
                lines = []
                out_lines = []
                f_path = "{0}/{1}".format(path, f)
                f_out_path = "{0}/{1}".format(output_path, f)
                with open(f_path, "r") as ff:
                    lines = ff.readlines()
                for line in lines:
                    cleaned = " ".join([stemmer.stem(clean_word(x)) for x in line.split()])
                    out_lines.append(cleaned)
                if len(lines) == len(out_lines):
                    if not os.path.isdir(output_path):
                        os.makedirs(output_path, mode=0o755)
                    print("Saving stemmed version of {0} to {1}".format(f_path, f_out_path))
                    pd.DataFrame(out_lines).to_csv(f_out_path, index=None, header=None)
                else:
                    print("Stemmed and non-stemmed version differ in line length")
                    print("NOT saving stemmed version of {0} to {1}".format(f_path, f_out_path))
    return


if __name__ == "__main__":
    stem_data()
