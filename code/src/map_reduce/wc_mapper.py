#!/usr/bin/env python2
"""wc_mapper.py"""
from __future__ import print_function
import sys


def clean_word(word):
    stripped = "".join(ch.lower() for ch in word if ch.isalpha())
    return stripped


stopwords = []
with open("english_stopwords.txt", "r") as f:
    stopwords = f.readlines()
    stopwords = [clean_word(word) for word in stopwords]


def is_legit_word(word):
    cond_1 = clean_word(word) not in stopwords
    cond_2 = len(word) > 1
    return cond_1 and cond_2


for line in sys.stdin:
    line = line.strip()
    words = line.split()
    filtered_words = [clean_word(word) for word in words if is_legit_word(word)]
    for word in filtered_words:
        # write the results to STDOUT (standard output);
        # what we output here will be the input for the
        # Reduce step, i.e. the input for reducer.py
        #
        # tab-delimited; the trivial word count is 1
        print('%s\t%s' % (word, 1))
