#!/usr/bin/env python2
from __future__ import print_function
import os
import sys

"""
Generates the each of the word pairs
Example: If there are 4 unique words, 
there will be a total of 4 * 3 = 12 pairs

Code example: echo 'foo bar foo fux qux' | python2 wo_mapper.py | sort -k1,1 | python2 wo_reducer.py
"""


def clean_word(word):
    stripped = "".join(ch.lower() for ch in word if ch.isalpha())
    return stripped


stopwords = []
with open("english_stopwords.txt", "r") as f:
    stopwords = f.readlines()
    stopwords = [clean_word(word) for word in stopwords]


def is_legit_word(word):
    cond_1 = clean_word(word) not in stopwords
    cond_2 = len(word) > 1
    return cond_1 and cond_2


words_to_count = []
files = [f for f in os.listdir(".") if f.endswith(".csv") and f.startswith("top_")]
if len(files) >= 1:
    file = files[0]
    lines = []
    with open(file, "r") as f:
        lines = f.readlines()
    for line in lines:
        line = line.strip()
        word, count = "", 0
        try:
            word, count = line.split(",")
            count = int(count)
        except ValueError:
            continue
        words_to_count.append(clean_word(word))
else:
    print("Top_N.csv not found in current directory!")
    sys.exit(3)

for line in sys.stdin:
    line = line.strip()
    words = line.split()
    idx = 0
    while idx < len(words):
        jdx = 0
        while jdx < len(words):
            if idx != jdx:
                first, second = clean_word(words[idx]), clean_word(words[jdx])
                if len(words_to_count) >= 1 and is_legit_word(first) and is_legit_word(second):
                    # if first in words_to_count or second in words_to_count:
                    if first in words_to_count:
                        print("{0},{1}\t{2}".format(first, second, 1))
            jdx += 1
        idx += 1
