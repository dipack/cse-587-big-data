import os
import time
import logging
import collections
import requests
import pandas as pd
from bs4 import BeautifulSoup as bs
from util import get_topic, rewind_clock, stem_str

logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p',
                    level=logging.INFO)


# TODO: Stem words while saving
class NYTeems:
    def __init__(self):
        self.api_key = "Cd8e2o84dbyQbPinZeiKVWxGxPrlVH1L"
        self.articles = None
        self.col_names = {"id": "_id", "url": "web_url", "timestamp": "pub_date", "text": "lead_paragraph"}

    def _search(self, term, page=1, from_date="20180101"):
        # Sleep time recommended by NYTimes is 6 seconds
        sleep_time = 7
        articles = collections.defaultdict(list)
        article_search_url = "https://api.nytimes.com/svc/search/v2/articlesearch.json?q={0}&begin_date={1}&api-key={2}&page={3}".format(
            term, from_date, self.api_key, page)
        try:
            response = requests.get(article_search_url)
            while response.status_code == 429:
                logging.info("Too many requests made to the NYTimes API! Sleeping for {0} seconds".format(sleep_time))
                time.sleep(sleep_time)
                logging.info("Awake!")
                response = requests.get(article_search_url)
            docs = response.json()['response']['docs']
        except KeyError as e:
            logging.error(e)
            logging.error("Error trying to look for {0}, on page {1} from date {2}".format(term, page, from_date))
            docs = []
        for doc in docs:
            try:
                if doc['type_of_material'].lower() not in ['video', 'op-ed', 'briefing', 'interactive',
                                                           'interactive feature']:
                    logging.debug("Type: {0}, Id: {1}".format(doc['type_of_material'], doc['_id']))
                    for col, actual_col in self.col_names.items():
                        if col == "text":
                            html = requests.get(doc["web_url"])
                            soup = bs(html.content, features="html.parser")
                            try:
                                article = soup.find('article', {'id': 'story'}).find('section', {'name': 'articleBody'})
                                if article is not None:
                                    # Stemming words as we save them
                                    stemmed = stem_str(article.text)
                                    articles[col].append(stemmed)
                                else:
                                    articles[col].append("")
                            except AttributeError:
                                articles[col].append("")
                        else:
                            # Do not save any metadata
                            # articles[col].append(doc[actual_col])
                            continue
            except KeyError:
                pass
        return articles

    def search(self, term, from_date="20180101", target_article_count=20):
        logging.info("Looking for {0} articles from {1} for term {2}".format(target_article_count, from_date, term))
        max_range = 10
        for page in range(max_range):
            logging.info("Scanning page: {0}".format(page))
            articles = self._search(term, page=page, from_date=from_date)
            if self.articles is None:
                self.articles = pd.DataFrame.from_dict(articles)
                # No metadata, so no id
                # self.articles.set_index('id', inplace=True)
            else:
                if len(articles):
                    df = pd.DataFrame.from_dict(articles)
                    # No metadata, so no id
                    # df.set_index('id', inplace=True)
                    self.articles = self.articles.append(df)
            logging.info("Have {0} articles currently".format(len(self.articles)))
        self.articles.replace('\n', ' ', regex=True, inplace=True)
        self.articles.drop_duplicates(inplace=True)
        trimmed = [x for x in self.articles.loc[:, "text"] if isinstance(x, str) and len(x) >= 1]
        self.articles = pd.DataFrame(pd.Series(trimmed), columns=["text"])
        logging.info("Have {0} unique articles".format(len(self.articles)))
        return self.articles

    def save_articles(self, target_filename="data/nyt_articles.csv"):
        if self.articles is not None:
            logging.info("Saving articles to file {0}".format(target_filename))
            # Only saving the text
            # In earlier iterations I was also saving metadata, but that is not required
            # So, I cut them out
            self.articles.to_csv(target_filename, sep=",", header=False, index=False)
        return


def main():
    nyt = NYTeems()
    counter = 0
    from_date = "20190101"
    target_article_count = 120

    stemmed_filename, non_stemmed, query_str = get_topic('nyt', 'tennis')

    if os.path.isfile(stemmed_filename):
        nyt.articles = pd.read_csv(stemmed_filename, header=None, index_col=False)
        logging.info(
            "Starting from where we left off before {0} with {1} articles".format(stemmed_filename, len(nyt.articles)))

    articles = nyt.search(query_str, from_date=from_date, target_article_count=target_article_count)
    counter += 1
    while len(nyt.articles) < target_article_count and counter < 10:
        nyt.save_articles(stemmed_filename)
        from_date = rewind_clock(from_date, -1)
        articles = nyt.search(query_str, from_date=from_date, target_article_count=target_article_count)
        counter += 1

    print(articles.head())
    nyt.save_articles(stemmed_filename)
    return


if __name__ == "__main__":
    main()
